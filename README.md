## 1 Development environment installation
### 1.1 Ubuntu development environment
The following packages have to be installed:
```shell
user@ubuntu:~$ sudo apt install default-jre git build-essential cmake libgtest-dev
```
GoogleTest package installed above must be built in order to use it:
```shell
user@ubuntu:~$ cd /usr/src/googletest
user@ubuntu:~$ sudo cmake .
user@ubuntu:~$ sudo cmake .
user@ubuntu:~$ sudo cmake --build . --target install
```
### 1.2 Eclipse
On Ubuntu Eclipse can be installed as snap package
```shell
user@ubuntu:~$ sudo snap install eclipse --edge --classic
```
Once Eclipse is installed it is recommended to install additional Eclipse plugins.
1. Open `Help` - `Eclipse Marketplace...`
2. Search for following plugins and make them installed:
    * The Complete Eclipse C/C++ IDE 9.4
    * CMake Editor
    * Bash Editor
    * FluentMark
3. Some additional features must be installed using standard Eclipse installation procedure.
    1. Open `Help` - `Install New Software...`
    2. Open `CDT 9.4 Update Site` (CDT version may differ based on the version installed)
    3. Install from `CDT Optional Features` section:
        * `C/C++ GCC Cross Compiler Support`
        * `C/C++ Unit Testing Support`
4. Turn off Eclipse launch bar in `Window` - `Preferences` - `Run/Debug` - `Launching` - `Launch Bar`
5. Turn off Eclipse theming in `Window` - `Preferences` - `General` - `Appearance`

## 2 ReVolta project setup
This section is not really mandatory to follow as all those settings are already done 
within the build in Eclipse project.

### 2.1 Repositories setup
Following these steps the project structure is setup including all the subrepositories used.
```shell
user@ubuntu:~/git/ReVolta$ git clone https://gitlab.com/ReVolta/ReVolta.git
user@ubuntu:~/git/ReVolta$ mkdir Source && cd Source
user@ubuntu:~/git/ReVolta$ git submodule add -b development https://gitlab.com/ReVolta/Source/Kernel.git
user@ubuntu:~/git/ReVolta$ git submodule add -b development https://gitlab.com/ReVolta/Source/Userspace.git
user@ubuntu:~/git/ReVolta$ cd ..
user@ubuntu:~/git/ReVolta$ git submodule add -b development https://gitlab.com/ReVolta/Script.git
user@ubuntu:~/git/ReVolta$ git submodule add -b development https://gitlab.com/ReVolta/Test.git
```

### 2.2 Eclipse project setup
1. Right click on ReVolta project - `New` - `Convert to C/C++ Project (Add C/C++ Nature)`
2. Select `C++`, `Makefile project`, `--Other Toolchain--`
3. Right click on ReVolta project - `Properties` - `Paths and Symbols` - `Source Location` - Add `/ReVolta/Kernel`
4. Add C++17 project nature:
    * Right click on ReVolta project - `Properties` - `C/C++ General` - `Preprocessor Include Paths` - `Providers` - `CDT Cross GCC Build-in Compiler Settings`:
      ```shell 
      ${COMMAND} ${FLAGS} -E -P -v -dD -std=c++17 "${INPUTS}"
      ```
    * Set C++ symbols: Right click on ReVolta project - `Properties` - `C/C++ General` - `Paths and Symbols` - `Symbols` - `Add...` :
      ```shell
      Symbol = __cplusplus
      Value = 201703L
      ```
5. CDT GCC Build output parser settings:
    - Right click on ReVolta project - `Properties` - `C/C++ General` - `Preprocessor Include Paths, Macros etc.` - `Providers (tab)` - `CDT GCC Build Output Parser`
        - Compiler command pattern:
    ```shell
    .*((gcc)|([gc]\+\+)|(clang))
    ```
        - Container to keep discovered entries: `Project`
        - Use heuristics to resolve paths: `Disable` (uncheck)
    - Make the provider top most in the providers list
6. Enable CDT GCC Builtin Compiler Settings:
    - Right click on ReVolta project - `Properties` - `C/C++ General` - `Preprocessor Include Paths, Macros etc.` - `Providers (tab)` - `CDT GCC Builtin 
      Compiler Settings`
        - Enable the provider (This is going to detect default compiler include paths automatically)
7. Toolchain settings:
    - Right click on ReVolta project - `Properties` - `C/C++ Build` - `Tool Chain Editor`
        - Uncheck `Display compatible toolchains only`
        - Select `Cross GCC` option
    Note: This settings will setup Eclipse build in compiler invocation commands and flags (${COMMAND},${FLAGS})


