cmake_minimum_required( VERSION 3.10 )

# Set CMake modules location
set( CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/Script/CMake/Modules )

# Set where the binaries and archives are built
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/Binary/${CMAKE_BUILD_TYPE} )
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/Binary/${CMAKE_BUILD_TYPE} )

# Set toolchain file
if( NOT ${CMAKE_SYSTEM_PROCESSOR} STREQUAL "" )
	# TODO: Rework the set below
	set( CMAKE_FIND_ROOT_PATH $ENV{HOME}/local/gcc-arm-none-eabi-8-2018-q4-major )
	
	set( CMAKE_TOOLCHAIN_FILE ${CMAKE_MODULE_PATH}/Toolchain/arm-none-eabi.cmake )
else()

    set( CMAKE_SYSTEM_PROCESSOR x86_64 )
    set( CMAKE_TOOLCHAIN_FILE ${CMAKE_MODULE_PATH}/Toolchain/x86_64-linux-gnu.cmake )
endif()

# The toolchain setup is done so create a project
project( ReVolta )

message( STATUS "Target system: ${CMAKE_SYSTEM_NAME}" )
message( STATUS "Target processor: ${CMAKE_SYSTEM_PROCESSOR}" )

# Manage build type
set( DEFAULT_BUILD_TYPE "Release" )

if( NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES )
  message( STATUS "Setting build type to '${DEFAULT_BUILD_TYPE}' as none was specified." )
  
  set( CMAKE_BUILD_TYPE ${DEFAULT_BUILD_TYPE} CACHE STRING "Choose the type of build." FORCE)
  
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

message( STATUS "Build type: ${CMAKE_BUILD_TYPE}" )

# Set ReVolta kernel root directory
set( REVOLTA_KERNEL_ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/Source/Kernel )
set( REVOLTA_USERSPACE_ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/Source/Userspace )

# Add ReVolta kernel source directory
add_subdirectory( ${REVOLTA_KERNEL_ROOT_DIR} )
#add_subdirectory( ${REVOLTA_USERSPACE_ROOT_DIR} )

# Enable / disable testing - defined in toolchain file (x86_64-linux-gnu)
if( ${REVOLTA_ENABLE_TESTING} )
    # Enable the CTest testing
    enable_testing()
    
    # Find Google Test package
    find_package( GTest REQUIRED )
    
    add_subdirectory( Test )
endif()
