#! /bin/sh

echo "Removing all unneccessary files and folders..."
find ./Binary/Debug/* -print -delete
find ./Binary/Release/* -print -delete
find ./Binary/ -print -delete
find ./Build/* -print -delete
find ./Build/ -print -delete
echo "...done."