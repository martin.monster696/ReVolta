#! /bin/sh
# Purpose: Prepare CMake build for current project
# Author: Martin Kopecky <martin.monster696@gmail.com> 

BUILD_PATH=Build
OUTPUT_PATH=Binary

for i in "$@"
do
case $i in
    -t=*|--target=*)
    TARGET_PROCESSOR="${i#*=}"
    shift # past argument=value
    ;;
    -b=*|--buildtype=*)
    BUILD_TYPE="${i#*=}"
    shift # past argument=value
    ;;
    *)
          # unknown option
    ;;
esac
done

if [ ! -d "$BUILD_PATH" ]; then
	mkdir ./$BUILD_PATH
fi

if [ ! -d "$OUTPUT_PATH" ]; then
	mkdir -p ./$OUTPUT_PATH/$BUILD_TYPE
fi

cmake -E chdir $BUILD_PATH cmake -G"Unix Makefiles" -DCMAKE_SYSTEM_PROCESSOR=${TARGET_PROCESSOR} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON ../